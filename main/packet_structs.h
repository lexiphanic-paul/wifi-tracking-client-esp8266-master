struct packet_result
{
    unsigned type:4;
    unsigned channel:8;
    uint8_t source[6];
    uint8_t target[6];
    signed rssi:8;
    unsigned :4;
};
