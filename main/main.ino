#include <ESP8266WiFi.h>
#include <WebSocketsClient.h>
#include "packet_structs.h"

const char* ssid     = "VM0733810";
const char* password = "hfk4kyxkGwvc";
char host[] = "192.168.0.36";
// const char* ssid     = "NoAppleDevicesAllowed";
// const char* password = "GuessThisBadAssPassword794613";
// char host[] = "192.168.1.13";
char path[] = "/";
WebSocketsClient webSocket;
bool ledState = false;
bool websocketInited = false;
bool websocketConnected = false;

// Use WiFiClient class to create TCP connections
WiFiClient client;


void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
  switch(type) {
    case WStype_DISCONNECTED:
      websocketConnected = false;
      break;
    case WStype_CONNECTED:
      websocketConnected = true;
      break;
    case WStype_TEXT:
      // Do nothing, we don't expect TEXT as that's too much overhead.
      break;
    case WStype_BIN:
      // @todo: Handle this.
      //USE_SERIAL.printf("[WSc] get binary length: %u\n", length);
      //hexdump(payload, length);

      // send data to server
      // webSocket.sendBIN(payload, length);
      break;
  }
}

void setup() {
    Serial.begin(115200);
    delay(10);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.swap();
    delay(5000);
}

int state = 0;
void loop() {
    // Do this first to ensure the buffer doesn't fill up.
    webSocket.loop();
    handleSerial();

    // Each step of the program is given a state number, I should change this to an ENUM but meh.
    if (state == 0) {

        // We start by connecting to a WiFi network
        // @todo: Remove this hardcoded setting and get it from memory or via the Wifi Config feature.
        WiFi.begin(ssid, password);
        state++;
    } else if (state == 1) {
        // We stay here until we're connected.
        delay(500);
        if (WiFi.status() == WL_CONNECTED) {
            state++;
        }
    } else if (state == 2) {
        // Connect to the websocket server
        digitalWrite(LED_BUILTIN, LOW);

        delay(500);
        if (websocketInited) {
            state++;
            return;
        }
        
        webSocket.begin(host, 8081, "/");
        webSocket.onEvent(webSocketEvent);
        webSocket.setReconnectInterval(5000);
        //webSocket.enableHeartbeat(15000, 3000, 2);
        websocketInited = true;
        
    } else if (state == 3) {
        if (websocketConnected) {
            state++;
        }
    } else if (state == 4) {
        // Our application is ready to proxy.
        if (!websocketConnected) {
            state++;
        }
    } else if (state == 5) {
        delay(1000);
        state++;
    } else {
        // @todo: Maybe do a power cycle here...
        state = 0;
    }
}

// Handle data coming from the other ESP-8266
unsigned type = 0;
unsigned packetLength = 0;
unsigned nullByteCount = 0;
bool nullPacketFound = false;
uint8_t packet[15];
void handleSerial() {
    byte datum;
    // while we have data
    while (Serial.available() > 0) {
        // Fetch it
        datum = Serial.read();
        if (state != 4) {
            // The program is not at the correct state so just drain the Buffer.
            continue;
        }

        if (datum == 0x1 && nullByteCount > 14) {
            nullPacketFound = true;
            nullByteCount = 0;
            type = 0;

            continue;
        }

        if (datum == 0x0) {
            ++nullByteCount;
        }

        if (!nullPacketFound) {
            continue;
        }

        // If our current packet type is 0 then this must be our packet key
        if (type == 0) {
            type = datum * 1;
            packet[0] = datum * 1;
            ++packetLength;
        } else if (type == 6) {
            packet[packetLength] = datum * 1;
            ++packetLength;

            if (packetLength == 15) {
                webSocket.sendBIN(packet, 15);
                type = 0;
                packetLength = 0;
            }
        } else {
            type = 0;
            packetLength = 0;
        }
    }
}
